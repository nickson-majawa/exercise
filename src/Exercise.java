
import com.creditdatamw.zerocell.annotation.Column;
import com.creditdatamw.zerocell.annotation.RowNumber;
import com.creditdatamw.zerocell.annotation.ZerocellReaderBuilder;

import java.time.LocalDate;

@ZerocellReaderBuilder("Exercise")
public class Exercise {
    @RowNumber
    private int rowNumber;

    @Column(index=0, name="FULL NAME")
    private String fullname;

    @Column(index=1, name="PHONE_NUMBER")
    private int phone_number;

    @Column(index=2, name="EMAIL")
    private String email;

    @Column(index=2, name="COMPANY_ACCOUNT_NO")
    private String company_account_no;

    @Column(index=2, name="ADDRES")
    private String address;

    // Getters and setters here ...

    public static void main(String... args) {
        // Then using the `Reader` class you can load 
        // a list from the excel file as follows:
        List<Exercise> people = Reader.of(Exercise.class)
                            .from(new File("directors.xlsx"))
                            .sheet("Sheet 1")
                            .list();

        // You can also inspect the column names of 
        // the class using the static `columnsOf` method:
        String[] columns = Reader.columnsOf(Exercise.class);    
    }
}